import { Database } from '../databases/database_abstract';
import { DatabaseInstanceStrategy } from '../database';

export class PersonsService {
    private readonly _db: Database;

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance();
    }

    public async getPersons() {
        return this._db.getPersons();
    }

}