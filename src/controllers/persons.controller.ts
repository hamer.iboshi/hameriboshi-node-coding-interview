import {
    JsonController,
    Get,
    Param,
    Put,
    Post,
    Body,
} from 'routing-controllers';
import { PersonsService } from '../services/persons.service';

@JsonController('/persons', {transformResponse: false})
export default class PersonsController {
    private _personsService: PersonsService;

    constructor() {
        this._personsService = new PersonsService();
    }

    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await this._personsService.getPersons(),
        }
    }
}

